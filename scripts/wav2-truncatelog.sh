#!/bin/sh
#########################################
#                                       #
# Truncate wav2 log to 2000 lines       #
# Truncate eb2 log to max 2000 lines    #
#                                       #
#########################################

tail /var/log/wav2/wav2-km.log -n 2000 > /var/log/wav2/wav2-km.tmp
mv /var/log/wav2/wav2-km.tmp /var/log/wav2/wav2-km.log
tail /var/log/wav2/eb2-km.log -n 2000 > /var/log/wav2/eb2-km.tmp
mv /var/log/wav2/eb2-km.tmp /var/log/wav2/eb2-km.log

tail /var/log/wav2/wav2-miles33.log -n 2000 > /var/log/wav2/wav2-miles33.tmp
mv /var/log/wav2/wav2-miles33.tmp /var/log/wav2/wav2-miles33.log
tail /var/log/wav2/eb2-miles33.log -n 2000 > /var/log/wav2/eb2-miles33.tmp
mv /var/log/wav2/eb2-miles33.tmp /var/log/wav2/eb2-miles33.log
