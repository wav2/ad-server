#!/bin/bash
####################################
#                                  #
# Unpack html5 published folder    #
# to Apache web folder for serving #
#                                  #
####################################
shopt -s nullglob
cd /home/miles33/www
for i in *.zip; do
dest="/var/www/html/miles33/${i:0:-4}"
  echo "Files found Processing `date`"
  unzip -o $i -d $dest
  mv $i ../archive/$i
  echo "Finished processing `date`"
done
