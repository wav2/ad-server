#!/bin/bash
####################################
# EasyBuild2                       #
# Unpack html5 published folder    #
# to Apache web folder for serving #
# And create web page to show Ads  #
####################################
shopt -s nullglob
cd /home/miles33/eb2
echo "Starting miles33 eb2 script"

#find . -type f -name "* *.zip" -exec bash -c 'mv "$0" "${0// /}"' {} \; 

DEST="/var/www/html/miles33/eb2"
#Protocol relative URL to allow for HTTP or HTTPS sites to show ads
URL="//adserver.adportalcloud.com/miles33/eb2"

for i in *.zip; do

  NAME="${i:0:-4}"  
  JOBNAME="`cut -d '_' -f1 <<< $i`"
  DIR="$JOBNAME"
  echo "unpacking $i"
  unzip -o "$i" -d "$DEST/$DIR" 
  HTML="`ls $DEST/$DIR/*.html`"
  HTML_FILE="${HTML##*-}"
  FORMAT="${HTML_FILE%*\.*}"
  echo "Copying html template"
  cp /home/wave2/files/ADPROOF_TEMP.html '/var/www/html/miles33/eb2/'"$JOBNAME"'.html'
  cp /home/wave2/files/ADTAGS_TEMP.html '/var/www/html/miles33/eb2/'"$JOBNAME"'-adtag.html'
  echo "Update Job Name"
  sed -i 's|__JOBNAME__|'"$JOBNAME"'|g' '/var/www/html/miles33/eb2/'"$JOBNAME"'.html'
  sed -i 's|__JOBNAME__|'"$JOBNAME"'|g' '/var/www/html/miles33/eb2/'"$JOBNAME"'-adtag.html'
  #Convert newlines to null so basename will work with names with spaces
  FILE=`ls "$DEST/$DIR/"*.html | tr '\n' '\0' | xargs -0 -n 1 basename`
  PROOF="`cat /home/wave2/files/"$FORMAT""Proof_TEMP" | sed 's/[&=<>]/\&/g'`"
  ADTAG="`cat /home/wave2/files/"$FORMAT""Adtag_TEMP"`"
  sed -i '/id="'$FORMAT'"/r '/home/wave2/files/"$FORMAT"Proof_TEMP'' '/var/www/html/miles33/eb2/'"$JOBNAME"'.html'
  sed -i 's|'"__"$FORMAT"__"'|'"$URL/$DIR/$FILE"'|' '/var/www/html/miles33/eb2/'"$JOBNAME"'.html'
  sed -i '/id="'$FORMAT'"/r '/home/wave2/files/"$FORMAT"Adtag_TEMP'' '/var/www/html/miles33/eb2/'"$JOBNAME"'-adtag.html'
  sed -i 's|'"__"$FORMAT"__"'|'"$URL/$DIR/$FILE"'|' '/var/www/html/miles33/eb2/'"$JOBNAME"'-adtag.html'
  mv "$i" "/home/miles33/archive/$i"
echo "Finished processing $i"

done
