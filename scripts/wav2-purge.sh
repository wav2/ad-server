#!/bin/bash

###########################
# Purge old archived zips #
# Files older than 8 days #
###########################

find /home/kmgroup/archive/*.zip -mtime +8 -exec rm {} \;
# find /var/www/html/kmgroup/* -mtime +365 -exec rm -r {} \;
find /home/miles33/archive/*.zip -mtime +8 -exec rm {} \;
